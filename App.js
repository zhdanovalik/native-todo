import React from 'react';
import { StyleSheet, Text, View, StatusBar, FlatList } from 'react-native';
import TodoInput from './components/TodoInput';

let todoId = 1;

export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            inputValue: '',
            todos: []
        };
    }

    onInputChange = evt => {
        this.setState({
            inputValue: evt.target.value
        });
    };

    onInputSubmit = () => {
        this.setState({
            todos: [
                ...this.state.todos,
                { id: todoId++, text: this.state.inputValue, completed: false }
            ]
        });
    };

    render() {
        return (
            <View style={styles.container}>
                <TodoInput
                    value={this.state.inputValue}
                    onChange={this.onInputChange}
                    onSubmit={this.onInputSubmit}
                />
                <Text>Open up App.js to start working on your app!</Text>
                <FlatList
                    data={this.state.todos}
                    renderItem={({ item }) => (
                        <Text key={item.id}>{item.text}</Text>
                    )}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#73c0ba',
        paddingTop: StatusBar.currentHeight,
        padding: 10
    }
});
