import React from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';

const TodoInput = ({ onChange, value, onSubmit }) => {
    return (
        <View style={styles.wrap}>
            <TextInput
                value={value}
                onChange={onChange}
                onSubmitEditing={onSubmit}
                placeholder="Add todo..."
                style={styles.input}
                autoFocus={true}
                underlineColorAndroid="transparent"
            />
        </View>
    );
};

const styles = StyleSheet.create({
    wrap: {
        marginTop: 10,
        padding: 10,
        backgroundColor: '#34978f'
    },
    input: {
        fontSize: 22,
        color: '#fff'
    }
});

export default TodoInput;
